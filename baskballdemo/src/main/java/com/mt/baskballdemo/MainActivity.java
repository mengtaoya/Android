package com.mt.baskballdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.mt.baskballdemo.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
ActivityMainBinding binding;
MyViewModel myViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //通过DataBindingUtil的setContentView方法或许布局
    binding= DataBindingUtil.setContentView(this,R.layout.activity_main);
    //获取MyViewModel对象
        myViewModel = ViewModelProviders.of(this).get(MyViewModel.class);
        //为xml绑定数据
        binding.setData(myViewModel);
        //设置观察，便于监听数据变化
        binding.setLifecycleOwner(this);

    }
}
