package com.mt.baskballdemo;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewModel extends ViewModel {
    private MutableLiveData<Integer> NumberA;
    private MutableLiveData<Integer> NumberB;
    private  int aBack,bBack;
    public MutableLiveData<Integer> getaNumber() {
        if(NumberA == null){
            //如果为空，就为Number赋值0
            NumberA = new MutableLiveData<>();
            NumberA.setValue(0);
        }
        return NumberA;
    }
    public MutableLiveData<Integer> getbNumber() {
        if(NumberB == null){
            //如果为空，就为Number赋值0
            NumberB = new MutableLiveData<>();
            NumberB.setValue(0);
        }
        return NumberB;
    }
    //加分
    public void addANumber(int n){
        aBack = NumberA.getValue();
        bBack = NumberB.getValue();
        NumberA.setValue(NumberA.getValue()+n);
    }
    public void addBNumber(int n){
        aBack = NumberA.getValue();
        bBack = NumberB.getValue();
        NumberB.setValue(NumberB.getValue()+n);
    }
    //清零
    public void  reset(){
        aBack = NumberA.getValue();
        bBack = NumberB.getValue();
        NumberA.setValue(0);
        NumberB.setValue(0);
    }
    //撤销
    public void  undo(){
        NumberA.setValue(aBack);
        NumberB.setValue(bBack);
    }
}
