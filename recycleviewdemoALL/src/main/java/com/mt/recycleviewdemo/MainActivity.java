package com.mt.recycleviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {
Button ver_btn,hor_btn,gride_btn,waterfall_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化控件
        init_view();
        //点击事件
        init_listener();
    }

    /**
     * 初始化控件
     */
    private void init_view() {

        ver_btn=findViewById(R.id.ver_btn);
        hor_btn=findViewById(R.id.hor_btn);
        gride_btn=findViewById(R.id.gride_btn);
        waterfall_btn=findViewById(R.id.waterfall_btn);
    }
    /**
     * 点击事件
     */
    private void init_listener() {
        ver_btn.setOnClickListener(this);
        hor_btn.setOnClickListener(this);
        gride_btn.setOnClickListener(this);
        waterfall_btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case  R.id.ver_btn:
                Intent intent=new Intent(MainActivity.this,HorRecycleActivity.class);
                startActivity(intent);
                break;
            case  R.id.hor_btn:
                Intent intent1=new Intent(MainActivity.this, VerRecycleActivity.class);
                startActivity(intent1);
                break;
            case  R.id.gride_btn:
                Intent intent2=new Intent(MainActivity.this, GirdRecycleActivity.class);
                startActivity(intent2);
                break;
            case  R.id.waterfall_btn:
                Intent intent3=new Intent(MainActivity.this, WaterFallActivity.class);
                startActivity(intent3);
                break;
        }
    }
}
