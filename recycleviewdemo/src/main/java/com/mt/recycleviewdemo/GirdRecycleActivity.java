package com.mt.recycleviewdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.mt.recycleviewdemo.adapter.GridAdapter;
import com.mt.recycleviewdemo.adapter.HorAdapter;

public class GirdRecycleActivity extends AppCompatActivity {
private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gird_recycle);
        recyclerView=findViewById(R.id.recyclerView2);
        //为网格布局
        recyclerView.setLayoutManager(new GridLayoutManager(GirdRecycleActivity.this,3));
        //为其设置适配器
       recyclerView.setAdapter(new GridAdapter(GirdRecycleActivity.this, new GridAdapter.OnItemClickListerner() {
           @Override
           public void onClick(int pos) {
               Toast.makeText(GirdRecycleActivity.this,"你点击了"+pos,Toast.LENGTH_SHORT).show();
           }
       }));
    }
}
