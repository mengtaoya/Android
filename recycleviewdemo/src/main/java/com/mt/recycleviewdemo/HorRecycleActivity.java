package com.mt.recycleviewdemo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Rect;
import android.os.Bundle;
import android.util.LayoutDirection;
import android.view.View;

import com.mt.recycleviewdemo.adapter.HorAdapter;

public class HorRecycleActivity extends AppCompatActivity {
RecyclerView hor_recycle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hor_recycle);
        //获取RecyclerView控件
        hor_recycle=findViewById(R.id.hor_recycle);
        //为线性布局
        hor_recycle.setLayoutManager(new LinearLayoutManager(HorRecycleActivity.this));
        //为其设置适配器
        hor_recycle.setAdapter(new HorAdapter(HorRecycleActivity.this));
 /*    //设置一个自定义的下滑线
        hor_recycle.addItemDecoration(new MyDirection());*/
    }
/*
    //设置一个下划线
    class MyDirection extends RecyclerView.ItemDecoration {
        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(0,0,0,getResources().getDimensionPixelOffset(R.dimen.dividerHeight));
        }
    }
*/

}
