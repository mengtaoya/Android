package com.mt.recycleviewdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.widget.Toast;

import com.mt.recycleviewdemo.adapter.WaterAdapter;

public class WaterFallActivity extends AppCompatActivity {
private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_fall);
        recyclerView=findViewById(R.id.recyclerView3);
        //瀑布流布局，2表示展示2列，后面的参数为水平展示还是垂直展示
        StaggeredGridLayoutManager staggeredGridLayoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setAdapter(new WaterAdapter(WaterFallActivity.this, new WaterAdapter.OnItemClickListerner() {
            @Override
            public void onClick(int pos) {
                Toast.makeText(WaterFallActivity.this,"你点击了"+pos,Toast.LENGTH_SHORT).show();
            }
        }));

    }
}
