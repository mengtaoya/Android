package com.mt.recycleviewdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mt.recycleviewdemo.R;

public class HorAdapter extends RecyclerView.Adapter<HorAdapter.MyViewHolder> {
    private Context mcontext;
    //构造方法
    public HorAdapter( Context context) {
        this.mcontext=context;
    }

    @NonNull
    @Override
    public HorAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //传入一个item布局
            return new MyViewHolder(LayoutInflater.from(mcontext).inflate(R.layout.hor_item,parent,false));
    }
//展示数据
    @Override
    public void onBindViewHolder(@NonNull HorAdapter.MyViewHolder holder, int position) {
holder.textView.setText("Hello ,Android");
    }

    @Override
    public int getItemCount() {
        //需要展示的个数
        return 30;
    }
    //需要定义一个内部类继承ViewHolder
    class  MyViewHolder extends RecyclerView.ViewHolder {
private TextView textView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            //注意这里是通过itemView来找，而不是R，因为我们的textView是在itemView里面的
            textView=itemView.findViewById(R.id.textView);
        }
    }

}
