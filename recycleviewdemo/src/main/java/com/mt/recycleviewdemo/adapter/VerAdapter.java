package com.mt.recycleviewdemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mt.recycleviewdemo.R;

public class VerAdapter extends RecyclerView.Adapter<VerAdapter.MyViewHolder>{
    private Context mcontext;
    private  OnItemClickListerner mlisterner;
    //构造方法
    public VerAdapter( Context context,OnItemClickListerner listerner) {
        this.mcontext=context;
        this.mlisterner=listerner;
    }
    @NonNull
    @Override
    public VerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mcontext).inflate(R.layout.ver_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull VerAdapter.MyViewHolder holder, final int position) {
        holder.textView.setText("Hello");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlisterner.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 25;
    }
    //ViewHolder内部类
    class MyViewHolder extends RecyclerView.ViewHolder{
private TextView textView;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.textView2);

        }
    }
    //点击事件的接口
    public  interface  OnItemClickListerner{
        void onClick(int pos);
    }
}
