package com.mt.recycleviewdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.mt.recycleviewdemo.adapter.VerAdapter;

public class VerRecycleActivity extends AppCompatActivity {
    public RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_recycle);
        recyclerView=findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(VerRecycleActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager( linearLayoutManager);
        recyclerView.setAdapter(new VerAdapter(VerRecycleActivity.this, new VerAdapter.OnItemClickListerner() {
            @Override
            public void onClick(int pos) {
                Toast.makeText(VerRecycleActivity.this,"你点击了"+pos,Toast.LENGTH_SHORT).show();
            }
        }));
    }
}
