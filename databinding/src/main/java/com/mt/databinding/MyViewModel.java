package com.mt.databinding;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewModel extends ViewModel {
    MutableLiveData<Integer> Number;

    public MutableLiveData<Integer> getNumber() {
        if(Number == null){
            Number = new MutableLiveData<>();
            Number.setValue(0);
        }
        return Number;
    }
    public  void addNumber(int n){
        Number.setValue(Number.getValue()+n);
    }
}
