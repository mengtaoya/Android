package com.mt.zfruitproject;


public class Fruit {
    private  String name;
    private  int imgageId;

    public Fruit(String name, int imgageId) {
        this.name = name;
        this.imgageId = imgageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgageId() {
        return imgageId;
    }

    public void setImgageId(int imgageId) {
        this.imgageId = imgageId;
    }
}
