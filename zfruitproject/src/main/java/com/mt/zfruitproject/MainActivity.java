package com.mt.zfruitproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    FloatingActionButton button;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    MyApdater apdater;
    private Fruit[] fruits = {new Fruit("Apple", R.drawable.img1), new Fruit("Banana", R.drawable.img2),
            new Fruit("Orange", R.drawable.img3),
            new Fruit("Apple", R.drawable.img1), new Fruit("Banana", R.drawable.img2),
            new Fruit("Orange", R.drawable.img3)
    };
    private List<Fruit> fruitList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //侧滑栏相关方法
        init_drawlayout();
        //floatButton
        init_floatButton();
        //初始化列表数据
        initFruits();
        //对recycleview的处理
        init_recycleview();
        //刷新数据
        init_refreshFruits();
    }




    /**
     * 对侧边栏的操作
     */
    private void init_drawlayout() {
         drawerLayout=findViewById(R.id.drawer_layout);
        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBar actionBar =getSupportActionBar();
        //再标题栏添加一个图片用于侧滑栏操作
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_input_get);
           /* drawerLayout.openDrawer(GravityCompat.START);*/
        }
        //默认第一个
        navigationView.setCheckedItem(R.id.nav_call);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }

    /**
     * 对悬浮按钮的操作
     */
    private void init_floatButton() {
        button = findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view,"你使用了Snackbar",Snackbar.LENGTH_SHORT).
                        setAction("确定", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(MainActivity.this,"你使用了Toast",Toast.LENGTH_SHORT).show();
                            }
                        }).show();
            }
        });
    }

    /**
     * recycleview相关操作
     */
    private void init_recycleview() {
        recyclerView=findViewById(R.id.recycler_view);
        GridLayoutManager  gird = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gird);
         apdater =new MyApdater(this,fruitList);
        recyclerView.setAdapter(apdater);

    }
    /**
     * 刷新数据
     */
    @SuppressLint("ResourceAsColor")
    private void   init_refreshFruits(){
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFruits();
            }
        });

    }
    /**
     * 初始化列表
     */
    private void initFruits() {
        fruitList.clear();
        for (int i = 0; i < 50; i++) {
            Random random = new Random();
            int index = random.nextInt(fruits.length);
            fruitList.add(fruits[index]);
        }
    }
    /**
     * 刷新数据
     */

    private void refreshFruits() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //注意要切回主线程进行数据更新
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        initFruits();
                        apdater.notifyDataSetChanged();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();
    }
}
